const express = require('express')
const app = express()
const passport = require('passport')
const cors = require('cors')
const config = require('./config')
const db = require('./db')
const bodyParser = require('body-parser')
const commentDal = require('./commentDal')
const jwtTools = require('./jwtTools')

const JwtStrategy = require('passport-jwt').Strategy
ExtractJwt = require('passport-jwt').ExtractJwt

passport.use(new JwtStrategy({
	secretOrKeyProvider: jwtTools.getSecret,
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}, (jwt_payload, done) => {done(null, jwt_payload.sub)}))

const port = process.env.PORT || config.port
const dbUrl = process.env.DB_URL || config.dbUrl

app.use(passport.initialize())
app.use(cors({
	origin: process.env.NODE_ENV === 'production' ? /(https?:\/\/opentgc\.com|https?:\/\/95\.216\.148\.182)/ : '*'
}))
app.use(bodyParser.json())

app.post('/post/:postId/comments', passport.authenticate('jwt', {session: false}), (req, res, next) => {
	if (!req.body.comment || !req.body.postDate)
		return next({statusCode: 400, extMessage: 'comment and postDate are required in body'})
	const commentObj = {
		comment: req.body.comment,
		author: req.user,
		date: new Date(),
		postId: req.params.postId,
		postDate: req.body.postDate
	}
	commentDal.saveComment(commentObj)
	.then(saveRes => saveRes.ops[0])
	.then(saveRes => res.json(saveRes))
	.catch(next)
})

app.get('/post/:postId/comments', (req, res, next) => {
	commentDal.getComments(req.params.postId)
	.then(comments => res.json(comments))
	.catch(next)
})

app.post('/bulk', (req, res, next) => {
	commentDal.getBulkCommentsForPosts(req.body)
	.then(comments => res.json(comments))
	.catch(next)
})

app.post('/bulkByAuthor', (req, res, next) => {
	commentDal.getBulkCommentsForAuthors(req.body)
	.then(comments => res.json(comments))
	.catch(next)
})

app.use((req, res) => {
	res.status(404).send('Page not Found')
})

app.use((err, req, res, next) => {
	const {extMessage, message, statusCode = 500} = err

	const logMessage = message || extMessage
	if (logMessage)
		console.error(logMessage)

	const myResponse = {}
	if (extMessage)
		myResponse.message = extMessage

	res.status(statusCode).json(myResponse)
})

db.connect(dbUrl)
.then(() => {
	app.listen(port, () => {
		console.log(`Started at port ${port}`)
	})
})
.catch(err => {
	console.error(err)
	process.exit(1)
})