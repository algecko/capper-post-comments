const db = require('./db')
const getCollection = () => db.get().collection('comments')

module.exports.saveComment = function (comment) {
	return getCollection().insert(comment)
	.catch(() => {throw {extMessage: 'Error saving Comment'}})
}

module.exports.getComments = function (postId) {
	return getCollection().find({postId}).toArray()
}

module.exports.getBulkCommentsForPosts = function (posts) {
	return getCollection().find({postId: {$in: posts}}).toArray()
}

module.exports.getBulkCommentsForAuthors = function (authors) {
	return getCollection().find({author: {$in: authors}}).toArray()
}