const fetch = require('node-fetch')
const CustomStrategy = require('passport-custom').Strategy

const config = require('./config')

function getTokenFromRequest (req) {
	const headers = req.headers
	const auth = headers.authorization
	if (!auth)
		return
	const authParts = auth.split(' ')
	if (authParts.length < 2 || authParts[0].toLowerCase() !== 'bearer')
		return
	return authParts[1]
}

const strategy = new CustomStrategy(function (req, done) {
	const token = getTokenFromRequest(req)

	if (!token)
		return done(null, false)

	fetch(`${config.authUrl}/token/verify`, {
		method: 'POST',
		body: JSON.stringify({token}),
		headers: {
			'content-type': 'application/json'
		},
		mode: 'cors'
	})
	.then(res => {
		if (res.status !== 200)
			return done(null, false)
		return res.json()
	})
	.then(body => {
		if (!body.valid)
			return done(null, false)
		done(null, body.token.body.sub)
	})
	.catch(err => {
		console.error(err)
		done(null, false)
	})

})

const dummySerializer = (user, done) => done(null, user)

module.exports = {strategy, serialize: dummySerializer, deserialize: dummySerializer}